import logging
import time
from threading import Thread

from src.mission_task import MissionTask
from src.example_tasks.first_task import FirstTask
from src.example_tasks.second_task import SecondTask
from src.setup_logging import setup_logging

logger = logging.getLogger()


class MissionManager(Thread):
    def __init__(self) -> None:
        super().__init__()
        self.current_task = FirstTask()

    def run(self) -> None:
        self.current_task.start()

        while True:
            if self.current_task.next_task_name != self.current_task.name:
                logger.critical("changing task")
                self.current_task.stop()

                if self.current_task.next_task_name == "FirstTask":
                    self.current_task = FirstTask()

                else:
                    self.current_task = SecondTask()

                self.current_task.start()

            time.sleep(1)


if __name__ == "__main__":
    setup_logging()

    mission_manager = MissionManager()
    mission_manager.run()
