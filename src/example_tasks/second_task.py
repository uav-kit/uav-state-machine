import logging
import random
import time

from src.mission_task import MissionTask

logger = logging.getLogger(__name__)


class SecondTask(MissionTask):
    def __init__(self) -> None:
        super().__init__()
        self.name = "SecondTask"
        self.next_task_name = self.name

    def _run(self) -> None:
        while True:
            logger.info("second task running")
            self.next_task_name = random.choice(["FirstTask", "SecondTask"])
            time.sleep(1)

            if self.task_end.is_set():
                return
